## Requirements

We're creating a search middleware that allows users to use text search for flights from and to Dubai Airports.

For example:
If the user searches: "_Flights from beirut today_"
The api should return the list of flights coming to Dubai from Beirut today.

If the user searches: "_Emirates flights to London_"
The api should return the expected departure time of Emirates flights from Dubai to London.

Or "_Flights to london on Wednesday_", or "_Weekend flights to Hong Kong_"

We're expecting the parser to understand:

* Basic words like _from_, _to_.

* Dates and day names, like _wednesday_, _weekend_, ...

* Cities like _Beirut_, _London_, _Budapest_, ...

* Carriers like _Emirates_, _FlyDubai_, ...

### Data

The Dubai Airports website provides an API endpoint to get Arrivals and Departures data.

```
GET http://www.dubaiairports.ae/api/flight/arrivals
GET http://www.dubaiairports.ae/api/flight/departures

Response:
{
  "status": true,
  "flights": [ //array of flights
    {
      uid": 216002,
      "type": "arrivals",
      "flightNumber": "EK958",
      "trafficTypeCode": "PS",
      "airlineCode": "UAE",
      "destinationCode": "DXB",
      "aircraftCode": "77W",
      "flightStatusCode": "EST",
      "originCode": "BEY",
      "beltNumber": "301",
      "gateNumber": null,
      "trafficTypeName": "Passenger Scheduled",
      "terminal": "3",
      "fullName": "Emirates",
      "fullNameA": "طيران الإمارات",
      "lang": {
        "en": {
          "airlineName": "Emirates",
          "originName": "Beirut",
          "destinationName": "Dubai",
          "flightStatusText": "Estimated"
        },
        "ae": {
          "airlineName": "طيران الإمارات",
          "originName": "بيروت",
          "destinationName": "دبي",
          "flightStatusText": "قدرت"
        }
      },
      "lastChanged": 1470141731, //Unix timestamp
      "scheduled": 1470142500, //Unix timestamp
      "estimated": 1470142260, //Unix timestamp
      "actual": null //Unix timestamp
    }
  ]
}

The only available parameter is limit (integer), which limits the number of flights returned.
```

### Backend

You're free to use any language/framework for the backend, be it PHP, Node.JS, or .NET.
Any way you go, the code needs to be well organized, commented, and tested.

#### Caching

The flights data gets updated once every minute, thus the api response should be cached on the server-side for 1 minute using any performant cache solution.

### Frontend

For the frontend, we're looking for an HTML page that calls the backend api via AJAX.

### Assets

In the provided folder you'll find the PSD, fonts and icons required for the design.
Icons need to be used as SVGs or iconfont.

### Delivery

The final solution needs to be hosted anywhere where it's accessible to test.
In addition you need to provide the full code export with instructions on how to run it.
